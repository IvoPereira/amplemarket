# HackerNews Scraper

*HackerNewsScraper* is a project that scrapes the latest 30 items from the main page of HackerNews.

## Requirements

 - PHP7
 - *curl* extension enabled
 - *composer*

## How to run?

You should type ``composer install`` in the root of the project so all the required libraries are automatically installed by *composer* - the most used PHP dependency manager.

To be able to access the project you should point your browser to the path where you installed the project.
