<?php
require "vendor/autoload.php";
use DiDom\Document;

$url = 'https://news.ycombinator.com';

// Our internal scraper that will do our business logic.
$scraper = new Scraper();

// Our third-party document parser.
$document = new Document($url, true);

$table_elements = $document->find('#hnmain table tr');

// The first item is relative to HackerNews header
// so we will exclude it
array_shift($table_elements);

// Let's subdivide our posts into chunks of 3 items
// as our post information is subdivided into 3 items as well.
// It would be easier to parse this way.
$chunks = array_chunk($table_elements, 3);

// The last chunk was relative to the "More" page. So let's remove it too.
array_pop($chunks);

$posts = [];

/**
 * We are going through each chunk, looking for the Post Rank, Title and URL
 * using an helper class that will contain our main business logic.
**/
foreach ($chunks as $chunk) {

  $scraper->scrapeChunk($chunk);
  $results = $scraper->getScrapeResults();

  // If some of our fields is empty, we don't want it.
  if (!$scraper->isValidScrapeResult()) {
    continue;
  }

  $posts[] = [
    'rank' => $scraper->getPostRank(),
    'title' => $scraper->getPostTitle(),
    'url' => $scraper->getPostUrl()
  ];

}

// As we now have our results in $posts, we can show them.
foreach ($posts as $post) {
  echo <<<HTML
{$post['rank']}º | <a href="{$post['url']}" title="{$post['title']}">{$post['title']}</a> - {$post['url']}
<br />
HTML;
}

/**
 * A better abstraction could be built on this class if it would be used
 * for more than one website.
**/
class Scraper {

  /**
   * This row will be searched for the needed informations.
   */
  private $row;

  private $postRank;
  private $postTitle;
  private $postUrl;

  public function setRow($row) {
    $this->row = $row;
  }

  private function scrapeRank() {
    if (empty($this->row->find('.rank')[0])) {
      return false;
    }

    $this->postRank = intval($this->row->find('.rank')[0]->text());
  }

  public function getPostRank(): int {
    return $this->postRank;
  }

  private function scrapeTitle() {
    if (empty($this->row->find('.title')[1])) {
      return false;
    }

    $this->postTitle = $this->row->find('.title')[1]->text();
  }

  public function getPostTitle(): string {
    return $this->postTitle;
  }

  private function scrapeUrl() {
    if (empty($this->row->find('.title a')[0])) {
      return false;
    }

    $this->postUrl = $this->row->find('.title a')[0]->getAttribute('href');
  }

  public function getPostUrl(): string {
    return $this->postUrl;
  }

  public function scrapeChunk(array $chunk): void {
    foreach ($chunk as $row) {
      $this->setRow($row);
      $this->scrapeRow();
    }
  }

  private function scrapeRow() {
    $this->scrapeRank();
    $this->scrapeTitle();
    $this->scrapeUrl();
  }

  public function isValidScrapeResult(): bool {
    return !empty($this->getPostRank()) && !empty($this->getPostTitle()) && !empty($this->getPostUrl());
  }

  public function getScrapeResults() {
    return [
      'rank' => $this->getPostRank(),
      'title' => $this->getPostTitle(),
      'url' => $this->getPostUrl()
    ];
  }

}
